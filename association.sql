DROP DATABASE IF EXISTS `association`;
create database `association`;
use association;

CREATE TABLE `association`.`Membre` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NOT NULL,
  `prenom` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `adresse` VARCHAR(45) NOT NULL,
  `description` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`id`));


CREATE TABLE `association`.`Commentaire` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `message` VARCHAR(128) NOT NULL,
  `pseudo` VARCHAR(45) NOT NULL,
  `jaime` INT,
  `date` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));
  
  INSERT INTO `commentaire` (`id`, `message`, `pseudo`, `date`) VALUES (NULL, 'Association incroyable !!! je recommande', 'UnMecContent', '05/06/2021 12:04');
  INSERT INTO `membre` (`id`, `nom`, `prenom`, `email`, `adresse`, `description`) VALUES (NULL, 'Jean', 'Michel', 'jean_mi@mail.com', 'rue des ponts', 'Membre actif ');