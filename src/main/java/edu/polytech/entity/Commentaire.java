package edu.polytech.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @Description Classe Entité Commentaire, correspondant à la table
 *              "commentaire" de la bdd
 */
@Entity
@Table(name = "commentaire")
public class Commentaire implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private String message;
	@Column
	private String pseudo;
	@Column
	private String date;
	@Column
	private int jaime;
	

	

	public Commentaire(int id, String message, String pseudo, String date,int jaime) {
		this.id = id;
		this.message = message;
		this.date = date;
		this.pseudo = pseudo;
		this.jaime = jaime;
	}

	public Commentaire() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public int getJaime() {
		return jaime;
	}

	public void setJaime(int jaime) {
		this.jaime = jaime;
	}

}
