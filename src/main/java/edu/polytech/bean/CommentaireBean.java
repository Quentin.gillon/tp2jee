package edu.polytech.bean;

import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import edu.polytech.entity.Commentaire;
import edu.polytech.models.CommentaireModel;

@SessionScoped
@ManagedBean(name = "commentaireBean")
public class CommentaireBean {

	private Commentaire commentaire;

	// Constructeurs

	public Commentaire getCommentaire() {
		return commentaire;
	}

	// Setters et Getters

	public void setCommentaire(Commentaire commentaire) {
		this.commentaire = commentaire;
	}

	public CommentaireBean() {
		this.commentaire = new Commentaire();
	}

	/*
	 * Sauvegarde un commentaire dans la BDD
	 * 
	 * @return La redirection de la page
	 */
	public String saveCommentaire() {
		
		CommentaireModel commentaireModel = new CommentaireModel();

		if (this.commentaire != null)
			commentaireModel.createCommentaire(this.commentaire);
		return "livret";
	}
	
	
	/*
	 * Ajout un like au commentaire dans la bdd
	 * 
	 * @return La redirection de la page
	 */
	public String addLike() {
		
		 FacesContext fc = FacesContext.getCurrentInstance();
	      Map<String,String> params = 
	       fc.getExternalContext().getRequestParameterMap();
	      String data =  params.get("id"); 
	      int id = Integer.parseInt(data);
		
		CommentaireModel commentaireModel = new CommentaireModel();
		Commentaire commentaire = commentaireModel.getCommentaireByID(id);		
		System.out.println(commentaire.getJaime());
		
		if (commentaire != null)
			commentaireModel.addLike(commentaire);
		return "livret";
	}

	/*
	 * Récupère la liste des commentaires de la BDD.
	 * 
	 * @return List<Commentaire> : La liste des commentaires présent dans la BDD
	 */
	public List<Commentaire> getAllCommentaires(){

		CommentaireModel commentaireModel = new CommentaireModel();
		List<Commentaire> listMembres = commentaireModel.getCommentaires();		
		return listMembres;
	}		

}
