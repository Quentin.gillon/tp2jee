package edu.polytech.bean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import edu.polytech.entity.Membre;
import edu.polytech.models.MembreModel;

@SessionScoped
@ManagedBean(name = "membreBean")
public class MembreBean {

	private Membre membre;

	// Constructeurs

	public MembreBean() {
		this.membre = new Membre();
	}

	// Setters et Getters

	public Membre getMembre() {
		return membre;
	}

	public void setMembre(Membre membre) {
		this.membre = membre;
	}

	/*
	 * Sauvegarde un membre dans la BDD
	 * 
	 * @return La redirection de la page
	 */
	public String saveMembre() {
		MembreModel membreModel = new MembreModel();
		if (this.membre != null)
			membreModel.createMembre(this.membre);
		return "presentation";
	}

	/*
	 * Récupère la liste des membres de la BDD.
	 * 
	 * @return List<Membre> : La liste des membres présent dans la BDD
	 */
	public List<Membre> getAllMembres() {
		MembreModel membreModel = new MembreModel();
		List<Membre> listMembres = membreModel.getMembres();
		return listMembres;
	}

}