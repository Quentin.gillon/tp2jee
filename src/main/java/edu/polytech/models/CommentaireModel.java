package edu.polytech.models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


import org.hibernate.query.Query;


import javax.transaction.Transactional;

import org.hibernate.Session;

import org.hibernate.Transaction;


import edu.polytech.entity.Commentaire;


public class CommentaireModel {

	/*
	 * Creer une transaction afin d'ajouter le commentaire dans la BDD
	 */
	public void createCommentaire(Commentaire commentaire) {
		SimpleDateFormat formater = null;
		Date aujourdhui = new Date();
		formater = new SimpleDateFormat("dd/MM/yyyy hh:mm");
		commentaire.setDate(formater.format(aujourdhui));
		commentaire.setJaime(0);
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			session.save(commentaire);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Creer une query afin de récupérer tous les commentaires de la BDD
	 * 
	 * @return La liste de tous les commentaire
	 */
	public List<Commentaire> getCommentaires() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			return session.createQuery("from Commentaire ORDER BY jaime DESC", Commentaire.class).list();
		}
	}
	
	/*
	 * Creer une query afin de récupérer un commentaires de la BDD selon l'id
	 * 
	 * @return Un commentaire
	 */
	public Commentaire getCommentaireByID(int id) {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			return (Commentaire) session.createQuery("from Commentaire WHERE id= :id", Commentaire.class).setParameter("id",id).uniqueResult();
		}
	}
	
	
	/*
	 * Creer une query afin d'ajouter un j'aime à un commentaires de la BDD selon l'id
	 * 
	 * @return Un commentaire
	 */
	@Transactional
	public void addLike(Commentaire commentaire)
	{	      
		int id = commentaire.getId();
		System.out.println("Mon id est : "+id);
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			  Transaction txn = session.beginTransaction();
			  Query query = session.createQuery("UPDATE  Commentaire SET jaime = jaime+1 WHERE id=:id");
			  query.setParameter("id",id);
			  query.executeUpdate();
			  txn.commit();
		      System.out.println("ExecuteLike");
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
	

