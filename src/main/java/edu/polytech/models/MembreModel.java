package edu.polytech.models;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import edu.polytech.entity.Membre;

public class MembreModel {

	/*
	 * Creer une transaction afin d'ajouter le membre dans la BDD
	 */
	public void createMembre(Membre membre) {

		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			session.save(membre);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Creer une query afin de récupérer tous les membres de la BDD
	 * 
	 * @return La liste de tous les membres
	 */
	public List<Membre> getMembres() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			return session.createQuery("from Membre", Membre.class).list();
		}
	}
}